var nib = require("nib");
var gulp = require("gulp");
var jst = require("gulp-jst");
var umd = require("gulp-umd");
var jade = require("gulp-jade");
var uglify = require('gulp-uglify');
var concat = require("gulp-concat");
var jshint = require("gulp-jshint");
var stylus = require("gulp-stylus");
var plumber = require("gulp-plumber");
var stylish = require("jshint-stylish");
var jstConcat = require('gulp-jst-concat');


var indexPath = "jade/index.jade";
var paths = {

	js: ["!js/app.js", "js/**/*.js"],

	styl: ["styl/**/*.styl"],

	jade: ["!" + indexPath, "jade/**/*.jade"],

	jade_index: indexPath

};

var onError = function(args) {
	console.log("============================= ERROR ===============================");
	console.log(args);
};


gulp.task('jade', function() {

	gulp.src(paths.jade_index)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(jade())
		.pipe(gulp.dest("./"))
		.pipe(plumber.stop());

	gulp.src(paths.jade)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(jade())
		.pipe(jstConcat("templates.js", {
			renameKeys: ['^.*jade(.*).html$', '$1']
		}))
		.pipe(gulp.dest("templates"))
		.pipe(plumber.stop());
});

gulp.task("scripts", function() {
	gulp.src(paths.js)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(jshint())
		.pipe(jshint.reporter(stylish))
		.pipe(umd())
		.pipe(concat("info-box.min.js"))
		.pipe(uglify())
		.pipe(gulp.dest('build'))
		.pipe(plumber.stop());
});
 
gulp.task('compress', function() {
  gulp.src('lib/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
});

gulp.task("styles", function() {
	gulp.src(paths.styl)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(stylus({
			use: nib(),
			compress: true
		}))
		.pipe(concat("info-box.min.css"))
		.pipe(gulp.dest("build"))
		.pipe(plumber.stop());
});

gulp.task("watch", function() {
	gulp.watch(paths.js, ["scripts"]);
	gulp.watch(paths.jade, ["jade"]);
	gulp.watch(paths.styl, ["styles"]);
});

gulp.task("deploy", ["scripts", "styles", "jade"]);
gulp.task("default", []);
