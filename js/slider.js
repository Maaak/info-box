'use strict';
var Slider = function() {
	this.default = {};
	this.default.slides = [];
	this.default.slidesURL = [];
	this.default.isCicled = true;
	this.default.currentSlideNumber = 0;
	this.render();
};

Slider.prototype.render = function() {
	var el = document.createElement("div");
	el.innerHTML = JST["info-box-nav"]();
	this.el = el.firstChild;


	this.prevEl = this.el.getElementsByClassName("prev")[0];
	this.nextEl = this.el.getElementsByClassName("next")[0];
	this.findEl = this.el.getElementsByClassName("find-store")[0];
	this.prevEl.addEventListener("click", this.prev.bind(this), false);
	this.nextEl.addEventListener("click", this.next.bind(this), false);

	return this.el;
};
Slider.prototype.first = function() {
	this.currentSlideNumber = 0;
	this.renderSlide(this.currentSlideNumber);
};

Slider.prototype.prev = function() {
	if (this.isCicled && --this.currentSlideNumber < 0) {
		this.currentSlideNumber = this.slides.length - 1;
	}
	this.renderSlide(this.currentSlideNumber);
};

Slider.prototype.next = function() {
	if (this.isCicled && ++this.currentSlideNumber === this.slides.length) {
		this.currentSlideNumber = 0;
	}
	this.renderSlide(this.currentSlideNumber);
};

Slider.prototype.last = function() {
	this.currentSlideNumber = this.slides.length - 1;
	this.renderSlide(this.currentSlideNumber);
};

Slider.prototype.renderSlide = function(slideNumber) {
	var el = document.body.getElementsByClassName("content-container")[0];
	if (el.firstChild) {
		el.removeChild(el.firstChild);
	}
	el.insertBefore(this.slides[slideNumber].render(), el.firstChild);
	this.findEl.setAttribute("href", this.slidesURL[slideNumber]);
};

Slider.prototype.setParams = function(args) {
	if (args) {
		this.slides = args.slides || this.slides || this.default.slides;
		this.slidesURL = args.slidesURL || this.slidesURL || this.default.slidesURL;
		this.isCicled = args.isCicled || this.isCicled || this.default.isCicled;
		this.currentSlideNumber = args.currentSlideNumber || this.currentSlideNumber || this.default.currentSlideNumber;
	} else {
		this.slides = this.default.slides;
		this.slidesURL = this.default.slidesURL;
		this.isCicled = this.default.isCicled;
		this.currentSlideNumber = this.default.currentSlideNumber;
	}
	this.renderSlide(0);
};