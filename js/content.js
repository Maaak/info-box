"use strict";
var Content = function(args){
	this.el = document.createElement("div");
	this.el.innerHTML = JST["info-box-content"]({model: args});
	this.el = this.el.firstChild;

	// initializing show|hide functional
	this.showDetailsEl = this.el.getElementsByClassName("show-details-button")[0];
	this.contentCoverEl = this.el.getElementsByClassName("content-cover")[0];
	this.contentImageEl = this.el.getElementsByClassName("content-image")[0];
	
	var self = this;
	this.clickHandler = function(){
		self.isShowenDetails = !self.isShowenDetails;
		self._show();
	};
	this.showDetailsEl.addEventListener("click", this.clickHandler, false);

	return this;
};

Content.prototype.render = function(){
	this.isShowenDetails = true;
	this._show();
	return this.el;
};

Content.prototype._show = function(){
	if (this.isShowenDetails) {
		this.el.classList.remove("full");
		this.showDetailsEl.textContent = "show details";
	}else{
		this.el.classList.add("full");
		this.showDetailsEl.textContent = "hide details";
	}
};